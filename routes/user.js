var express = require("express");
var router = express.Router();
var passport = require("passport");
var bcrypt = require("bcrypt");
var User = require("../models/user");

router.get("/signup", function (req, res, next) {
  res.render("signup");
});
router.post("/signup", async (req, res) => {
  const { password, email } = req.body;
  const hash = await bcrypt.hash(password, 12);
  const user = new User({
    email,
    password: hash,
  });
  await user.save();
  res.redirect("/");
});

router.get("/login", (req, res) => {
  res.render("login");
});

router.post("/login", async (req, res) => {
  const { username, password } = req.body;
  const user = await User.findOne({ username });
  const validPassword = await bcrypt.compare(password, user.password);
  if (validPassword) {
    res.redirect("/");
  } else {
    res.redirect("/user/login");
  }
});

module.exports = router;
